package channel;

import managers.ChannelManager;
import managers.PlayerManager;
import objects.BSPlayer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class FormatChannelCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		BSPlayer p = PlayerManager.getPlayer(sender);
		if (args.length == 0) {
			ChannelManager.setChannelFormat(sender, p.getChannelName());
			return true;
		} else {
			return false;
		}
	}

}
